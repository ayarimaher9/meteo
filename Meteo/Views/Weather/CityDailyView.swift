//
//  CityDailyView.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI
import MeteoSDK

struct CityDailyView : View {
    
    @State var day: MeteoData
    
    var body: some View {
        ZStack {
            HStack(alignment: .center) {
                Text("\(day.dt)")
                Spacer()
                HStack(spacing: 16) {
                    verticalTemperatureView(min: true)
                    verticalTemperatureView(min: false)
                }
            }
        }
    }
    
    func verticalTemperatureView(min: Bool) -> some View {
        VStack(alignment: .trailing) {
            Text(min ? "min" : "max")
                .font(.footnote)
                .foregroundColor(.gray)
            Text(min ? "\(day.temp)" : "\(day.feels_like)")
                .font(.headline)
        }
    }
    
}
