//
//  CityHourlyView.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI
import MeteoSDK

struct CityHourlyView : View {
    
    @ObservedObject var city: City
    
    private let rowHeight: CGFloat = 110
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 16) {
                ForEach(city.weather?.data ?? []) { hour in
                    VStack(spacing: 16) {
                        Text("\(hour.dt)")
                            .font(.footnote)
                        Image(systemName: "moon.stars.fill")
                            .font(.body)
                        Text("\(hour.temp)")
                            .font(.headline)
                    }
                }
            }
            .padding([.trailing, .leading])
        }
        .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
        .padding([.top, .bottom])
    }
    
}
