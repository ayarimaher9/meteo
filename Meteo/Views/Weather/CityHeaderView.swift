//
//  CityHeaderView.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//
import SwiftUI
import MeteoSDK

struct CityHeaderView: View {
    
    @ObservedObject var city: City
    
    var temperature: String {
        guard let temperature = city.weather?.data.first?.temp else {
            return "-ºC"
        }
        return "\(temperature)"
    }
    
    var body: some View {
        HStack(alignment: .center) {
            Spacer()
            HStack(alignment: .center, spacing: 16) {
                Image(systemName: "cloud.moon.fill")
                    .font(.largeTitle)
                Text(temperature)
                    .font(.largeTitle)
            }
            Spacer()
        }
        .frame(height: 110)
    }
}
