//
//  CityRow.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI
import MeteoSDK

struct CityRow : View {
    
    @ObservedObject var city: City
    
    var body: some View {
        NavigationLink(destination: CityWeatherView(city: city)) {
            HStack(alignment: .firstTextBaseline) {
                Text(city.name)
                    .lineLimit(nil)
                    .font(.title)
                Spacer()
                HStack {
                    Image(systemName: "cloud.moon.fill")
                        .foregroundColor(Color.gray)
                        .font(.title)
                    Text("-ºC")
                        .foregroundColor(.gray)
                        .font(.title)
                }
            }
            .padding([.trailing, .top, .bottom])
        }
    }
    
}
