//
//  WeatherList.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI

extension Weather {
    
    struct List<T: Codable & Identifiable>: Codable {
        
        var list: [T]
        
        enum CodingKeys: String, CodingKey {
            
            case list = "data"
            
        }
        
    }
    
}
