//
//  Weather.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import Foundation

struct Weather: Codable {
    
    var current: HourlyWeather
    var hours: Weather.List<HourlyWeather>
    var week: Weather.List<DailyWeather>
    
    enum CodingKeys: String, CodingKey {
        
        case current = "currently"
        case hours = "hourly"
        case week = "daily"
        
    }
    
}
