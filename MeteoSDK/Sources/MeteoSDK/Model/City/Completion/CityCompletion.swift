//
//  CityCompletion.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI
import Combine

public class CityCompletion: NSObject, ObservableObject {
    
    public var completionManager: CityCompletionManager
        
    @Published public var predictions: [CityCompletion.Prediction] = []
    
    public override init() {
        predictions = []
        completionManager = CityCompletionManager()
        super.init()
    }
    
    public func search(_ search: String) {
        completionManager.getCompletion(for: search) { (predictions) in
            DispatchQueue.main.async {
                self.predictions = predictions
            }
        }
    }
    
}
