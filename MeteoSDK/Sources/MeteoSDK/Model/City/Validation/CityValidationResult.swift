//
//  CityValidationResult.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI

extension CityValidation {
    
    public  struct Result: Codable {
        
        public var cityData: CityData
        
        enum CodingKeys: String, CodingKey {
            
            case cityData = "result"
            
        }
        
    }
    
    public struct CityData: Codable {
        
        var name: String
        var geometry: Geometry
        
        struct Geometry: Codable {
            
            var location: Location
            
            struct Location: Codable {
                
                var longitude: Double
                var latitude: Double
                
                enum CodingKeys: String, CodingKey {
                    
                    case longitude = "lng"
                    case latitude = "lat"
                    
                }
                
            }
            
        }
        
    }
    
}
