//
//  CityStore.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI
import Combine

public class CityStore: ObservableObject {
        
    @Published public var cities: [City] = [City()]
    
}
