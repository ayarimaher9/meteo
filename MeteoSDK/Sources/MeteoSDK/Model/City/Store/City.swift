//
//  City.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import SwiftUI
import Combine

public class City: ObservableObject{
        
    public var name: String
    public var longitude: Double
    public var latitude: Double
    
    @Published public var image: UIImage?
    @Published public var weather: MeteoResponse?
    
    init() {
        self.name = "Paris"
        self.longitude = 5.915807
        self.latitude = 45.572353
        self.image = nil
        self.weather = nil
        self.getWeather()
    }
    
    init(cityData data: CityValidation.CityData) {
        self.name = data.name
        self.longitude = data.geometry.location.longitude
        self.latitude = data.geometry.location.latitude
        self.image = nil
        self.weather = nil
        self.getWeather()
    }
    
    private func getWeather() {
        WeatherManager.getWeather(for: self) { (weather) in
            DispatchQueue.main.async {
                self.weather = weather
            }
        }
    }
    
}
