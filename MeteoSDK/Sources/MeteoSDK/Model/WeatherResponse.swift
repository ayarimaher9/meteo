//
//  WeatherResponse.swift
//  Weather
//
//  Created by Maher on 27/02/2023.
//  Copyright © 2023 Maher. All rights reserved.
//

import Foundation

public struct MeteoResponse: Codable {
    public let lat: Float
    public let lon: Float
    public let timezone: String
    public let timezone_offset: Int
    public let data: [MeteoData]
}

public struct MeteoData: Codable, Identifiable {
    public var id = UUID()
    public let dt: Int
    public let sunrise: TimeZone
    public let sunset: TimeZone
    public let temp: Float
    public let feels_like: Float
    public let pressure: Int
    public let humidity: Int
    public let dew_point: Float
    public let uvi: Float
    public let clouds: Int
    public let visibility: Int
    public let wind_speed: Float
    public let wind_deg: Int
    public let weather: [Wheather]
}

public struct Wheather: Codable, Identifiable {
    public let id: Int
    public let main: String
    public let description: String
    public let icon: String
}
