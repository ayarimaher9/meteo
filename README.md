# MeteoSDK

MeteoSDK is available as a method to see the wheater of the city you add.

###  Installation

1. Using Xcode go to File > Swift Packages > Add Package Dependency


###  How to use

Inject the MeteoSDK module dependancy in your  `SceneDelegate`  like below:

```swift
 
 import MeteoSDK
 
         let cityStore = MeteoSDK().cityStore
```


# Note

You need to update "meteoKey" and "googleMaps" in NetworkManager class with your created api keys
